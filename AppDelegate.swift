
import UIKit

class AppDelegate : ESAppDelegate {

    var db: FMDatabase;
    var currentUserID: Int;

    override init() {
        var path = NSFileManager.defaultManager()
            .URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true, error: nil)?
            .URLByAppendingPathComponent("SwissArmyPod.db").path
        db = FMDatabase(path: path)
        currentUserID = 1;
        super.init();
    }

    override func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {
        db.open()
        // set up the signed in view controller
        var storyboard = UIStoryboard(name:"iPhoneStoryboard", bundle:nil)
        var vc : AnyObject! = storyboard.instantiateViewControllerWithIdentifier("SignedInViewController")
        if let rootVC = self.window.rootViewController as? UINavigationController {
            rootVC.viewControllers = [vc]
        }

        return true
    }

    override func applicationWillTerminate(application: UIApplication) {
        db.close();
    }
}
