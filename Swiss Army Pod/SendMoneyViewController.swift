//
//  SendMoneyViewController.swift
//  Swiss Army Pod
//
//  Created by Josh Stewart on 10/24/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

import UIKit

class SendMoneyViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var amountTextField: UITextField!
    var amount: Int = 0

    // UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        self.amountTextField.becomeFirstResponder()
    }

    // UITextFieldDelegate

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            amount = amount / 10
        } else { // TODO: check if string is a number, they might have a hardware keyboard attached
            var newAmount: Int = (amount * 10) + string.toInt()!
            if newAmount < 10000 {
                amount = newAmount
            }
        }

        textField.text = NSString(format: "$%.2f", (Float(amount) / 100.0)) as String
        return false
    }

    // IBActions

    @IBAction func settleAction(sender: UIButton) {
        self.amountTextField.resignFirstResponder()
        let alert = UIAlertController(title:"settle \(amountTextField.text) with {friend}?", message: nil, preferredStyle:.Alert)
        alert.addAction(UIAlertAction(title: "no", style: .Cancel) { (action) -> Void in
            self.amountTextField.becomeFirstResponder()
            return
        })
        alert.addAction(UIAlertAction(title: "yes", style: .Default) { (action) -> Void in
            var response = RestForIOS.postJSONWithData([
                "sender" : "",
                "receiver" : "",
                "amount" : self.amount
            ], andURL: "http://gossamer.extropicstudios.com/balance/settle") { error -> Void in
                NSLog("error: \(error)")
                // TODO: error handler
                return
            }
            NSLog("\(response)")
            self.navigationController?.popViewControllerAnimated(true)
            return
        })
        presentViewController(alert, animated: true, completion: nil)
    }

    @IBAction func submitAction(sender: UIButton) {
        self.amountTextField.resignFirstResponder()
        let alert = UIAlertController(title:"send \(amountTextField.text) to {friend?}", message:nil, preferredStyle:.Alert)
        alert.addAction(UIAlertAction(title: "no", style: .Cancel) { (action) -> Void in
            self.amountTextField.becomeFirstResponder()
            return
        })
        alert.addAction(UIAlertAction(title: "yes", style: .Default) { (action) -> Void in
            var response = RestForIOS.postJSONWithData([
                "sender" : "",
                "receiver" : "",
                "amount" : self.amount
            ], andURL: "http://gossamer.extropicstudios.com/balance/send", andCallback: { error -> Void in
                NSLog("error: \(error)")
                // TODO: error handler
                return
            })
            NSLog("\(response)")
            self.navigationController?.popViewControllerAnimated(true)
            return
        })
        presentViewController(alert, animated: true, completion: nil)
    }

}