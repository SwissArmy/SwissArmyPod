//
//  AmountField.swift
//  Swiss Army Pod
//
//  Created by Josh Stewart on 10/25/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

import UIKit

class AmountField: UITextField {
    override func caretRectForPosition(position: UITextPosition!) -> CGRect {
        return CGRectZero
    }
}