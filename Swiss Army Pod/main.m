//
//  main.m
//  Swiss Army Pod
//
//  Created by Joshua Stewart on 5/14/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Swiss_Army_Pod-Swift.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
