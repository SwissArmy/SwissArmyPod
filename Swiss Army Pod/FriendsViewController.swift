//
//  FriendsViewController.swift
//  Swiss Army Pod
//
//  Created by Josh Stewart on 10/24/14.
//  Copyright (c) 2014 Extropic Studios. All rights reserved.
//

import UIKit

class FriendsViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {

    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    var friendsList = [
        ("mathias", 4000),
        ("sandy", 0),
        ("jeff", -2000),
    ]
    var addFriendTextField: UITextField?

    // UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        var results = appDelegate.db.executeQuery("SELECT * FROM friends WHERE user_id = ?", withArgumentsInArray: [appDelegate.currentUserID])

        // TODO: display some kind of loading spinner
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> () in
            var response = RestForIOS.postJSONWithData([
                "sender" : "",
            ], andURL: "http://gossamer.extropicstudios.com/balance/friends") { error -> Void in
                NSLog("error: \(error)")
                // TODO: error handler
                return
            }
            // TODO: parse response to friends
            return
        })
        // TODO: style table cells to include friend picture & balance total
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
    }

    // UITableViewDataSource

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friendsList.count;
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // TODO: need to figure out why i cant dequeue by reuse identifier
        var cell = UITableViewCell(style: .Value1, reuseIdentifier: "cell")
        var owed = friendsList[indexPath.row].1
        cell.textLabel!.text = "\(friendsList[indexPath.row].0)"
        cell.detailTextLabel?.font = UIFont.systemFontOfSize(12)
        if owed > 0 {
            var amount = NSString(format: "$%.2f", (Float(owed) / 100.0))
            cell.detailTextLabel?.textColor = UIColor(red: 0.8, green: 0.0, blue: 0.0, alpha: 1)
            cell.detailTextLabel?.text = "you owe \(amount)"
        } else if owed < 0 {
            var amount = NSString(format: "$%.2f", (Float(-owed) / 100.0))
            cell.detailTextLabel?.textColor = UIColor(red: 0.0, green: 0.8, blue: 0.0, alpha: 1)
            cell.detailTextLabel?.text = "they owe \(amount)"
        } else {
            cell.detailTextLabel?.text = "balance"
        }

        return cell
    }

    // UITableViewDelegate

    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        // TODO: pass friend data to next controller
        self.performSegueWithIdentifier("SendMoneySegue", sender: self)
        return indexPath
    }

    // IBActions

    @IBAction func addFriendSelector(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "add new friend", message: nil, preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "name"
            self.addFriendTextField = textField
        }
        alert.addAction(UIAlertAction(title: "cancel", style: .Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "add", style: .Default) { (action) -> Void in
            // TODO: save in core data and send to server
            //self.friendsList.append(("\(self.addFriendTextField!.text)", ""))
            return
        })
        presentViewController(alert, animated: true, completion: nil)
    }
}